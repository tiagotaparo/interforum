﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterForum.Models
{
    public class Tags
    {
        public int id_tag { get; set; }
        public string nome { get; set; }
        public int id_disciplina { get; set; }
    }
}