﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ForumFatec.Models
{
    public class DisciplinaModel : ModelBase
    {
        public List<Disciplina> Read()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT DisciplinaId, Nome from Disciplina";

            SqlDataReader reader = cmd.ExecuteReader();

            List<Disciplina> lista = new List<Disciplina>();

            while (reader.Read())
            {
                Disciplina d = new Disciplina();

                d.DisciplinaId = (int)reader["DisciplinaId"];
                d.Nome = (string)reader["Nome"];

                lista.Add(d);
            }
            return lista;
        }

        public void Create(Disciplina d)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Disciplina VALUES (@nome)";

            cmd.Parameters.AddWithValue("@nome", d.Nome);

            cmd.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "DELETE FROM Disciplina WHERE DisciplinaId = " + id;

            cmd.ExecuteNonQuery();
        }

        public Disciplina Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM Disciplina  WHERE DisciplinaId = @id";

            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                Disciplina d = new Disciplina();
                d.DisciplinaId = (int)reader["DisciplinaId"];
                d.Nome = (string)reader["Nome"];

                return d;
            }

            return null;
        }

        public void Update(Disciplina d)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE Disciplina SET Nome = @nome WHERE DisciplinaId = @id";

            cmd.Parameters.AddWithValue("@nome", d.Nome);
            cmd.Parameters.AddWithValue("@id", d.DisciplinaId);


            cmd.ExecuteNonQuery();
        }
    }
}