﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ForumFatec.Models
{
    public class PostagemModel : ModelBase
    {
        public List<Postagem> Read()
        {
            var lista = new List<Postagem>();

            string sql = "SELECT * FROM Postagem";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var p = new Postagem();
                p.PostagemId = (int)reader["PostagemId"];
                p.Titulo = (string)reader["Titulo"];
                p.Conteudo = (string)reader["Conteudo"];
                p.UserId = (int)reader["UserId"];

                lista.Add(p);
            }
            return lista;
        }

        public Postagem Read(int id)
        {
            string sql = "SELECT * FROM Postagem WHERE PostagemId = @id";


            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            Postagem postagem = new Postagem();

            if (reader.Read())
            {
                postagem.PostagemId = (int)reader["PostagemId"];
                postagem.Titulo = (string)reader["Titulo"];
                postagem.Conteudo = (string)reader["Conteudo"];

            }


            return postagem;
        }

        public void Create(Postagem p)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Postagem VALUES (@titulo,@conteudo,@userid,null)";
            cmd.Parameters.AddWithValue("@titulo", p.Titulo);
            cmd.Parameters.AddWithValue("@conteudo", p.Conteudo);
            cmd.Parameters.AddWithValue("@userid", 1);
            cmd.ExecuteNonQuery();
        }

        public void Update(Postagem p)
        {
            string sql = "UPDATE Postagem SET Titulo = @titulo, Conteudo = @conteudo WHERE PostagemId = @id";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            cmd.Parameters.AddWithValue("@id", p.PostagemId);
            cmd.Parameters.AddWithValue("@titulo", p.Titulo);
            cmd.Parameters.AddWithValue("@conteudo", p.Conteudo);

            cmd.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM Postagem WHERE PostagemId = @id";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            cmd.Parameters.AddWithValue("@id", id);

            cmd.ExecuteNonQuery();
        }
    }
}