﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient; //Add

namespace ForumFatec.Models
{
    public class CursoModel : ModelBase
    {
        public List<Curso> Read()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT * from Curso";

            SqlDataReader reader = cmd.ExecuteReader();

            List<Curso> lista = new List<Curso>();

            while (reader.Read())
            {
                Curso c = new Curso();

                c.CursoId = (int)reader["CursoId"]; // é mesmo que ->c.CursoId = reader.GetInt32(0);
                c.Nome = (string)reader["Nome"];
                c.Periodo = (string)reader["Periodo"];
                c.Coordenador = (string)reader["Coordenador"];

                lista.Add(c);
            }
            return lista;
        }

        public void Create(Curso c)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Curso VALUES (@nome, @periodo, @coordenador)";

            cmd.Parameters.AddWithValue("@nome", c.Nome);
            cmd.Parameters.AddWithValue("@periodo", c.Periodo);
            cmd.Parameters.AddWithValue("@coordenador", c.Coordenador);

            cmd.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "DELETE FROM Curso WHERE CursoId = " + id;

            cmd.ExecuteNonQuery();
        }

        public Curso Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "SELECT * FROM Curso  WHERE CursoId = @id";

            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                Curso c = new Curso();
                c.CursoId = (int)reader["CursoId"];
                c.Nome = (string)reader["Nome"];
                c.Periodo = (string)reader["Periodo"];
                c.Coordenador = (string)reader["Coordenador"];
                return c;
            }

            return null;
        }

        public void Update(Curso c)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE Curso SET Nome = @nome, Periodo = @periodo, Coordenador = @coordenador WHERE CursoId = @id";

            cmd.Parameters.AddWithValue("@nome", c.Nome);
            cmd.Parameters.AddWithValue("@periodo", c.Periodo);
            cmd.Parameters.AddWithValue("@coordenador", c.Coordenador);
            cmd.Parameters.AddWithValue("@id", c.CursoId);


            cmd.ExecuteNonQuery();
        }
    }
}