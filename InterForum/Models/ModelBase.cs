﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ForumFatec.Models
{
    public abstract class ModelBase : IDisposable
    {
        //TODO Adicionar no arquivo de confuguração (web.config)
        const string SERVIDOR = "localhost";
        const string BANCODADOS = "BdForumFatec";
        const string USUARIO = "sa";
        const string SENHA = "dba";

        //Pertence ao namespace System.Data.SqlClient
        protected SqlConnection connection;


        public ModelBase()
        {
            string strConn = $"Data Source={SERVIDOR}; Initial Catalog={BANCODADOS}; Integrated Security=true";
            // "User Id={USUARIO}; Password={SENHA}";

            //Criando objeto que conecta com o banco
            connection = new SqlConnection(strConn);

            //Abrindo conecção com o banco
            connection.Open();

        }

        public void Dispose()
        {
            // dispose é a ultima instrução "coisa" a ser feita pela classe, depois ela morre !
            connection.Close();
        }

        /*  internal static void Create(Categoria c)
          {
              throw new NotImplementedException();
          }´*/
    }


}