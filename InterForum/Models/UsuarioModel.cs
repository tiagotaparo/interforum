﻿using ForumFatec.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace InterForum.Models
{
    public class UsuarioModel : ModelBase
    {

        public List<Usuario> Read()
        {
            string sql = "SELECT * FROM tb_usuario";

            //SqlCommand cmd = new SqlCommand(sql, connection); --- Outra Opção
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            SqlDataReader reader = cmd.ExecuteReader();

            List<Usuario> lista = new List<Usuario>();

            while (reader.Read())
            {
                Usuario usuario = new Usuario();
                usuario.UsuarioId = reader.GetInt32(0); //Indice 0 ou seja, coluna 0 da tebela
                usuario.Nome = reader.GetString(1); //Indice 1 ou seja, coluna 1 da tebela
                usuario.Email = reader.GetString(2); //Indice 0 ou seja, coluna 0 da tebela
                usuario.Senha = reader.GetString(3); //Indice 1 ou seja, coluna 1 da tebela
                usuario.Inativo = reader.GetString(4);

                lista.Add(usuario); //Cria a Lista
            }

            return lista; //Retornar a Lista
        }

        public Usuario Read(string email, string senha)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = @"SELECT * FROM Users
                               WHERE email = @email AND senha = @senha";

            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@senha", senha);

            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                //existe um cliente com este email/senha:
                Usuario usuario = new Usuario();
                usuario.UsuarioId = (int)reader["UserId"];
                usuario.Nome = (string)reader["Nome"];
                usuario.Email = (string)reader["Email"];
                usuario.Senha = (string)reader["Senha"];

                return usuario;
            }
            else
            {
                //não encontrou um cliente com este email/senha:
                return null;
            }
        }

        public void Delete(int id)
        {
            string sql = "UPDATE Users SET inativo = 1 WHERE id = @id";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            cmd.ExecuteNonQuery();
        }

        public void Create(Usuario u)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Users VALUES (@nome, @email, @senha, @inativo)";

            string inativo = "0";
            cmd.Parameters.AddWithValue("@nome", u.Nome);
            cmd.Parameters.AddWithValue("@email", u.Email);
            cmd.Parameters.AddWithValue("@senha", u.Senha);
            cmd.Parameters.AddWithValue("@inativo", inativo);

            cmd.ExecuteNonQuery();
        }

        public void Update(Usuario u)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "UPDATE Users SET nome = @nome, senha = @senha WHERE id = @id";

            cmd.Parameters.AddWithValue("@nome", u.Nome);
            cmd.Parameters.AddWithValue("@senha", u.Senha);
            cmd.Parameters.AddWithValue("@id", u.UsuarioId);

            cmd.ExecuteNonQuery();
        }

    }
}