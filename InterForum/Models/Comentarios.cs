﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterForum.Models
{
    public class Comentarios
    {
        public int ComentarioId { get; set; }
        public string Conteudo { get; set; }
        public int Postagem_id { get; set; }
        public int User_id { get; set; }
        public string Data { get; set; }
    }
}