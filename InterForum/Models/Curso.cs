﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ForumFatec.Models
{
    public class Curso
    {
        public int CursoId { get; set; }
        [MaxLength(100)]
        public string Nome { get; set; }
        [MaxLength(100)]
        public string Periodo { get; set; }
        [MaxLength(100)]
        public string Coordenador { get; set; }
    }
}