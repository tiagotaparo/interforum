﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForumFatec.Models
{
    //POCO
    public class Disciplina
    {
        public int DisciplinaId { get; set; }
        public string Nome { get; set; }
    }
}