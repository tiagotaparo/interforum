﻿using ForumFatec.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace InterForum.Models
{
    public class ComentariosModel : ModelBase
    {
        public List<Comentarios> Read(int id)
        {
            var lista = new List<Comentarios>();

            string sql = "SELECT * FROM Comentarios WHERE postagem_id = @id ORDER BY data DESC";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;
            cmd.Parameters.AddWithValue("@id", id);

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var p = new Comentarios();
                p.ComentarioId = (int)reader["ComentarioId"];
                p.Conteudo = (string)reader["conteudo"];
                p.Postagem_id = (int)reader["postagem_id"];
                p.User_id = (int)reader["user_id"];

                lista.Add(p);
            }
            return lista;
        }

        public void Create(Comentarios c)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = "INSERT INTO Comentarios VALUES (@conteudo,@user_id,@postagem_id,@data)";
            cmd.Parameters.AddWithValue("@conteudo", c.Conteudo);
            cmd.Parameters.AddWithValue("@postagem_id", c.Postagem_id);
            cmd.Parameters.AddWithValue("@user_id", c.User_id);
            cmd.Parameters.AddWithValue("@data", System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            cmd.ExecuteNonQuery();
        }

        public void Update(Comentarios c)
        {
            string sql = "UPDATE Comentarios SET conteudo = @conteudo WHERE postagem_id = @postagem_id AND ComentarioId = @comentario_id";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            cmd.Parameters.AddWithValue("@comentario_id", c.ComentarioId);
            cmd.Parameters.AddWithValue("@postagem_id", c.Postagem_id);
            cmd.Parameters.AddWithValue("@conteudo", c.ComentarioId);

            cmd.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            string sql = "DELETE FROM Comentarios WHERE ComentarioId = @id";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = sql;

            cmd.Parameters.AddWithValue("@id", id);

            cmd.ExecuteNonQuery();
        }
    }
}