﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumFatec.Models;

namespace ForumFatec.Controllers
{
    public class DisciplinaController : Controller
    {

        public ActionResult Read()
        {
            using (DisciplinaModel model = new DisciplinaModel())     // Abre conexão
            {
                List<Disciplina> lista = model.Read();           // Usa conexão
                ViewBag.lista = lista;

            } //model.Dispose();                            // Fecha conexão

            return View();
        }


        public ActionResult Delete(int id)
        {
            using (DisciplinaModel model = new DisciplinaModel()) //Métodos com apenas uma instrução não precisa de {}
                model.Delete(id);

            return RedirectToAction("Read");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Create(FormCollection form)
        {
            string nome = form["Nome"]; //<input type="text" name="Nome" ...

            Disciplina d = new Disciplina();
            d.Nome = nome;

            using (DisciplinaModel model = new DisciplinaModel())
            {
                model.Create(d);
            }

            return RedirectToAction("Read");
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            using (DisciplinaModel model = new DisciplinaModel())
            {
                ViewBag.disciplina = model.Read(id);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Update(FormCollection form)
        {
            string nome = form["Nome"];
            int id = int.Parse(form["DisciplinaId"]);

            Disciplina d = new Disciplina();
            d.Nome = nome;
            d.DisciplinaId = id;

            using (DisciplinaModel model = new DisciplinaModel())
            {
                model.Update(d);
            }

            return RedirectToAction("Read");
        }
    }
}