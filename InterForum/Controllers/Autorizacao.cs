﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InterForum.Controllers
{
    public class Autorizacao : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(filterContext.HttpContext.Session["usuario"] == null)
            {
                filterContext.Result = new RedirectResult("/usuario/login");
            }
            base.OnActionExecuting(filterContext);
        }
    }
}