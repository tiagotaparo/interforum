﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InterForum.Models;

namespace InterForum.Controllers
{
    public class UsuarioController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        // GET: Usuario
        public ActionResult CadLogin()
        {
            ViewBag.NaoExiste = false;
            return View();
        }

        [HttpPost]
        public ActionResult CadLogin(FormCollection form)
        {
            string email = form["Email"];
            string senha = form["Senha"];

            using (var model = new UsuarioModel())
            {
                Usuario user = model.Read(email, senha);

                if (user != null)
                {
                    Session["usuario"] = user;
                    return RedirectToAction("Index","Postagem");
                }
                else
                {
                    ViewBag.NaoExiste = true;
                    return View();
                }
            }
        }

        public ActionResult Delete(int id)
        {
            //Efetuar a exclusão da Categoria
            using (UsuarioModel model = new UsuarioModel())
                model.Delete(id);

            return RedirectToAction("");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            string nome = form["Nome"];
            string email = form["Email"];
            string senha = form["Senha"];

            Usuario u = new Usuario();
            u.Nome = nome;
            u.Email = email;
            u.Senha = senha;

            using (UsuarioModel model = new UsuarioModel())
            {
                model.Create(u);
            }

            ViewBag.Mensagem = "Usuário cadastrado com sucesso";

            return RedirectToAction("CadLogin");

        }

        public ActionResult Update(string email, string senha)
        {
            using (UsuarioModel model = new UsuarioModel())
            {
                ViewBag.usuario = model.Read(email, senha);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Update(FormCollection form)
        {

            string nome = form["Nome"];
            string senha = form["Senha"];

            int id = int.Parse(form["UsuarioId"]);


            Usuario u = new Usuario();
            u.Nome = nome;
            u.Senha = senha;
            u.UsuarioId = id;

            using (UsuarioModel model = new UsuarioModel())
            {
                model.Update(u);
            }

            return RedirectToAction("");
        }

    }
}