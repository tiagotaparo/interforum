﻿using InterForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InterForum.Controllers
{
    public class ComentariosController : Controller
    {
        // GET: Comentarios
        public ActionResult Index()
        {
            return View();
        }


        [ValidateInput(false)]
        public void Create(Comentarios comentario)
        {
            // Pegando ID do usuário no backend por motivos de segurança
            var user = (Usuario)Session["usuario"];
           
            comentario.User_id = user.UsuarioId;

            using (var c = new ComentariosModel() )
            {
                
                c.Create(comentario);
                // RedirectToAction comentado pois esse método será chamado por requisição assíncrona
               // return RedirectToAction("PostDetail", "Postagem",new { id = comentario.Postagem_id });
            }
            
        }
    }
}