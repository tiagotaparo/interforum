﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumFatec.Models;
using InterForum.Controllers;
using InterForum.Models;

namespace ForumFatec.Controllers
{
    public class PostagemController : Controller
    {

        [Autorizacao]
        public ActionResult PostDetail(int id)
        {
            using (PostagemModel model = new PostagemModel())
            {
                var teste = Session["usuario"];
                var postagem = model.Read(id);
                ViewBag.postagem = postagem;

            }
            using(ComentariosModel comentarios = new ComentariosModel())
            {
                var comment = comentarios.Read(id);
                ViewBag.comentarios = comment;

            }
            

            return View();
        }

        [Autorizacao]
        // GET: Postagem
        public ActionResult Index()
        {
            using (var model = new PostagemModel())
            {
                var postagens = model.Read();
                return View(postagens);
            }
        }

        [HttpGet]
        [Autorizacao]
        public ActionResult Create()
        {
            using (var model = new PostagemModel())
            {
                var postagem = model.Read();
                return View();
            }
            
        }

        [HttpPost]
        [Autorizacao]
        public ActionResult Create(Postagem p)
        {
            using (var model = new PostagemModel())
            {
                model.Create(p);
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        [Autorizacao]
        public ActionResult Update(int id)
        {
            using (PostagemModel model = new PostagemModel())
            {
                var postagem = model.Read(id);
                return View(postagem);
            }
        }

        [HttpPost]
        [Autorizacao]
        public ActionResult Update(FormCollection form)
        {
            string titulo = form["Titulo"];
            string conteudo = form["Conteudo"];
            FormCollection teste = form;
            int postagemid = int.Parse(form["PostagemId"]);

            Postagem p = new Postagem();

            p.Titulo = titulo;
            p.Conteudo = conteudo;
            p.PostagemId = postagemid;
            using (PostagemModel model = new PostagemModel())
            {
                model.Update(p);
            }

            return RedirectToAction("Index");
        }


        [HttpGet]
        [Autorizacao]
        public ActionResult Delete(int id)
        {
            using ( var model = new PostagemModel())
            {
                model.Delete(id);
                return RedirectToAction("Index");
            }
        }

    }
}