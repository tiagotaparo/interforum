﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForumFatec.Models; //Add

namespace ForumFatec.Controllers
{
    public class CursoController : Controller
    {
        // GET: Categoria
        public ActionResult Read()
        {
            using (CursoModel model = new CursoModel())     // Abre conexão
            {
                List<Curso> lista = model.Read();           // Usa conexão
                ViewBag.lista = lista;

            } //model.Dispose();                            // Fecha conexão

            return View();
        }

        //localhost:####/curso/delete/

        public ActionResult Delete(int id)
        {
            using (CursoModel model = new CursoModel()) //Métodos com apenas uma instrução não precisa de {}
                model.Delete(id);

            return RedirectToAction("Read");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Create(FormCollection form)
        {
            string nome = form["Nome"]; //<input type="text" name="Nome" ...
            string periodo = form["Periodo"];
            string coordenador = form["Coordenador"];

            Curso c = new Curso();
            c.Nome = nome;
            c.Periodo = periodo;
            c.Coordenador = coordenador;


            using (CursoModel model = new CursoModel())
            {
                model.Create(c);
            }

            return RedirectToAction("Read");
        }

        [HttpGet]
        public ActionResult Update(int id)
        {
            using (CursoModel model = new CursoModel())
            {
                ViewBag.curso = model.Read(id);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Update(FormCollection form)
        {
            string nome = form["Nome"];
            string periodo = form["Periodo"];
            string coordenador = form["Coordenador"];
            int id = int.Parse(form["CursoId"]);

            Curso c = new Curso();
            c.Nome = nome;
            c.Periodo = periodo;
            c.Coordenador = coordenador;
            c.CursoId = id;

            using (CursoModel model = new CursoModel())
            {
                model.Update(c);
            }

            return RedirectToAction("Read");
        }
    }
}