﻿ClassicEditor
    .create(document.querySelector('#editor'))
    .catch(error => {
        console.error(error);
    }).then(editor => {
        console.log('Editor was initialized', editor);
        ckEditor = editor;
    })
    .catch(err => {
        console.error(err.stack);
    });




$("#postarComentario").on('click', function (e) {
    e.preventDefault();

    let postagemId = $("#postagemid").val();
    let conteudoComentario = ckEditor.getData();
    
    $.ajax({
        method: "POST",
        url: "/Comentarios/Create",
        data: {
            postagem_id: postagemId,
            conteudo: conteudoComentario
        }
    });
});

